/*
 Copyright 2017 Thomás Sousa Silva.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

$.fn.isVisibleOnScreenTop = function (pageTopOffset = 0, bottomOffset = pageTopOffset) {
    var element = $(this);
    var pageTop = ($(window).scrollTop() + pageTopOffset);
    var elementTop = element.offset().top;
    var elementBottom = (elementTop + element.height() + bottomOffset);
    return ((pageTop >= elementTop) && (elementBottom >= pageTop));
};

$.fn.isVisibleOnScreen = function (fully = false, topOffset = 0, bottomOffset = 0) {
    var element = $(this);
    var w = $(window);
    var pageTop = w.scrollTop();
    var pageBottom = (pageTop + w.height());
    var elementTop = element.offset().top + topOffset;
    var elementBottom = (elementTop + element.height() + bottomOffset);
    if (fully) {
        return ((elementTop >= pageTop) && (elementBottom <= pageBottom));
    }
    return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
};

$.fn.spy = function (handler, topOffset, bottomOffset, fully = false, onlyFirstView = false, screenTop = false) {
    $(this).each(function () {
        var element = $(this);
        var listener = function () {
            var visible = (screenTop ? element.isVisibleOnScreenTop(topOffset, bottomOffset) : element.isVisibleOnScreen(fully, topOffset, bottomOffset));
            if (element.visible !== visible) {
                if (visible && (element.firstView === undefined)) {
                    element.firstView = true;
                } else if (element.firstView === true) {
                    element.firstView = false;
                }
                var firstView = (element.firstView === true);
                if ((!onlyFirstView) || (onlyFirstView && firstView)) {
                    handler(visible, firstView, element);
                }
                element.visible = visible;
                if (onlyFirstView && firstView) {
                    $(window).unbind("scroll", listener);
                    $(window).unbind("click", listener);
                    $(window).unbind("resize", listener);
                }
            }
        };
        listener();
        $(window).scroll(listener);
        $(window).click(listener);
        $(window).resize(listener);
    });
};

$.fn.spyScreenTop = function (handler, topOffset, bottomOffset, fully, onlyFirstView) {
    $(this).spy(handler, topOffset, bottomOffset, fully, onlyFirstView, true);
};

$.fn.spyFirstView = function (handler, topOffset, bottomOffset, fully, screenTop) {
    $(this).spy(handler, topOffset, bottomOffset, fully, true, screenTop);
};

$.fn.spyFirstViewOverScreenTop = function (handler, topOffset, bottomOffset, fully) {
    $(this).spyFirstView(handler, topOffset, bottomOffset, fully, true);
};

$.fn.loadSource = function (src, removeAttributes = true) {
    var o = (this);
    o.attr("src", src);
    if (removeAttributes) {
        o.removeAttr("lazy-src loadOnClick loadOnFirstView topOffset bottomOffset");
    }
    return this;
};

$.fn.loadSourceOnFirstView = function (src, topOffset = - 150, bottomOffset = (topOffset * - 1), removeAttributes = true) {
    $(this).spyFirstView(function (visible, firstView, element) {
        element.loadSource(src, removeAttributes);
    }, topOffset, bottomOffset);
    return this;
};

$.fn.loadSourceOnClick = function (src, clickableElement, removeAttributes = true) {
    var o = $(this);
    var listener = function () {
        o.loadSource(src, removeAttributes);
        $(clickableElement).unbind("click", listener);
    };
    $(clickableElement).click(listener);
    return this;
};

$.fn.registerLazySourceLoading = function () {
    $(this).each(function () {
        var o = $(this);
        var src = o.attr("lazy-src");
        if (src !== undefined) {
            var selector = o.attr("loadOnClick");
            var loadOnFirstView = (o.attr("loadOnFirstView") !== undefined);
            if ((selector !== undefined) && (selector.length !== 0)) {
                o.loadSourceOnClick(src, selector);
            } else {
                loadOnFirstView = true;
            }
            if (loadOnFirstView) {
                var topOffset = parseInt(o.attr("topOffset"));
                if (isNaN(topOffset)) {
                    topOffset = undefined;
                }
                var bottomOffset = parseInt(o.attr("bottomOffset"));
                if (isNaN(bottomOffset)) {
                    bottomOffset = undefined;
                }
                o.loadSourceOnFirstView(src, topOffset, bottomOffset);
            }
        }
    });
    return this;
};

$.registerLazySourceLoading = function (selector = "[lazy-src]") {
    $(selector).registerLazySourceLoading();
};
