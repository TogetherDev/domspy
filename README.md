# DOM Spy #

#Versions

* **1.0**

## What is it?

This is a utility library for facilitate the espionage of html elements. It's builded with the [JQuery](https://jquery.com/).

## Using Nav Handler

You can download it and later add to your project.

## Use example:

Import the Nav.js file to your html page and in your main.js file you can call the desired functions.

### Html page

```
#!html

<!DOCTYPE html>
<html>
    <head>
        <title>Nav Example</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <img lazy-src="resources/img/image1.jpg">
        <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
        <script src="resources/js/Spy.js" type="text/javascript"></script>
        <script src="resources/js/main.js" type="text/javascript"></script>
    </body>
</html>

```

### main.js

```
#!javascript

$(function () {
    $.registerLazySourceLoading();
});
```

## Licensing

**DOM Spy** is provided and distributed under the [Apache Software License 2.0](http://www.apache.org/licenses/LICENSE-2.0).

Refer to *LICENSE.txt* for more information.
